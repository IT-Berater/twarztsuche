import React from "react";
import { Arzt } from "./Arzt";
import { Krankenhaus } from "./Krankenhaus";
import { Einsatz } from "./Einsatz";
import { Fachbereich } from "./Fachbereich";
import { Suchbegriffe } from "./Suchbegriffe";
import "./Karte.css";

export class Karte extends React.Component {
  render() {
    if (this.props.details === 1) {
      return (
        <div className="daten-satz" key={this.props.satz.arztNr.toString()}>
          <Arzt satz={this.props.satz} />
          <Krankenhaus satz={this.props.satz} />
        </div>
      );
    } else if (this.props.details === 2) {
      return (
        <div className="list-tabelle" key={this.props.satz.arztNr.toString()}>
          <table>
            <tr>
              <td class="breite-100">{this.props.satz.arztNr}</td>
              <td class="breite-300">{this.props.satz.arztName}</td>
              <td class="breite-300">{this.props.satz.fachgebiet}</td>
              <td class="breite-100">{this.props.satz.anästhesieKooperativ}</td>

              <td class="breite-500">
                <a href={this.props.satz.krhsUrl}>{this.props.satz.krhsName}</a>
              </td>
            </tr>
          </table>
        </div>
      );
    } else {
      return (
        <div className="daten-satz" key={this.props.satz.arztNr.toString()}>
          <Arzt satz={this.props.satz} />
          <Krankenhaus satz={this.props.satz} />
          <div className="email ">{this.props.satz.eMail}</div>
          <Fachbereich satz={this.props.satz} />
          <Einsatz satz={this.props.satz} />
          <div>{this.props.satz.arztSpezielleKompetenz}</div>
          <div className="focus">{this.props.satz.focus}</div>
          <div className="kommentar">{this.props.satz.kommentar}</div>
          <Suchbegriffe satz={this.props.satz} />
          <div className="betreuer">{this.props.satz.betreuer}</div>
        </div>
      );
    }
  }
}
