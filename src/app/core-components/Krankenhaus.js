import React from "react";
import "./Krankenhaus.css";

export class Krankenhaus extends React.Component {
  render() {
    return (
      <>
        <div className="bold">{this.props.satz.krhsName}</div>
        <div className="description">
          {this.props.satz.krhsStrNr}
          <br></br>
          {this.props.satz.krhsPlz} {this.props.satz.krhsOrt}
          <br></br>
          Tel. {this.props.satz.krhsTelNr}
          <br></br>
        </div>
        <a className="link" href={this.props.satz.krhsUrl}>
          <p>{this.props.satz.krhsUrl}</p>
        </a>
      </>
    );
  }
}
