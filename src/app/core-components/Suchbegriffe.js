import React from "react";
import "./Suchbegriffe.css";

export class Suchbegriffe extends React.Component {
  render() {
    var isSuchergebnis =
      String(this.props.satz.arztSuchbegriffe).length > 0 ? true : false;

    if (isSuchergebnis) {
      return (
        <>
          <div className="bold">Suchbegriffe:</div>
          {this.props.satz.arztSuchbegriffe}
        </>
      );
    } else {
      return null;
    }
  }
}
