import React from "react";
import "./Fachbereich.css";

export class Fachbereich extends React.Component {
  render() {
    return (
      <div>
        <table class="fachbereich">
          <tbody>
            <tr>
              <td>{this.props.satz.fachgebiet}</td>
              <td>{this.props.satz.fachgebiet1}</td>
              <td>{this.props.satz.fachgebiet2}</td>
              <td>{this.props.satz.fachgebiet3}</td>
            </tr>
          </tbody>
        </table>
        <br></br>
      </div>
    );
  }
}
