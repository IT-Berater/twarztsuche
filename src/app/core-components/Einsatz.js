import React from "react";
import "./Einsatz.css";

export class Einsatz extends React.Component {
  render() {
    return (
      <div>
        <table class="description">
          <tbody>
            <tr>
              <th>Anästh.: {this.props.satz.anästhesieKooperativ}</th>
              <th>Erw.</th>
              <th>Kinder</th>
              <th>Neugeb.</th>
              <th>RAB</th>
            </tr>
            <tr>
              <td>Planbar</td>
              <td>{this.props.satz.erwPlan}</td>
              <td>{this.props.satz.kindPlan}</td>
              <td>{this.props.satz.kleinKindPlan}</td>
              <td>{this.props.satz.rab}</td>
            </tr>
            <tr>
              <td>Notfall</td>
              <td>{this.props.satz.erwNot}</td>
              <td>{this.props.satz.kindNot}</td>
              <td>{this.props.satz.kleinKindNot}</td>
              <td>{this.props.satz.rab}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
