import React from "react";
import "./Kopfzeile.css";

export class Kopfzeile extends React.Component {
  render() {
    return (
      <header>
        <div className="container">
          <h1>{this.props.titel}</h1>
        </div>
      </header>
    );
  }
}
