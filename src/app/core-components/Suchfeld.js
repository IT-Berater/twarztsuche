import React from "react";
import "./Suchfeld.css";

export class Suchfeld extends React.Component {
  render() {
    return (
      <div>
        <div className="arztNummer">
          Treffer: {this.props.ergebnis.length} von {this.props.satz.length}
        </div>
      </div>
    );
  }
}
