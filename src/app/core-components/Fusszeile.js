import React from "react";
import "./Fusszeile.css";

export class Fusszeile extends React.Component {
  render() {
    return (
      <footer>
        <div className="container">
          <p>{this.props.titel}</p>
        </div>
      </footer>
    );
  }
}
