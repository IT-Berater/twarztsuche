import React from "react";
import "./Arzt.css";

export class Arzt extends React.Component {
  render() {
    return (
      <>
        <div className="arztNummer">{this.props.satz.arztNr}</div>
        <div className="normal">
          {this.props.satz.arztFunktion} {this.props.satz.arztTitel}
          {this.props.satz.arztVorname} {this.props.satz.arztName}
        </div>
      </>
    );
  }
}
