import React, { useState } from "react";
import "./App.css";
import Fuse from "fuse.js";
import ärzte from "./data.json";

import { Kopfzeile } from "./app/core-components/Kopfzeile";
import { Fusszeile } from "./app/core-components/Fusszeile";
import { Karte } from "./app/core-components/Karte";
import { Suchfeld } from "./app/core-components/Suchfeld";

const options = {
  // isCaseSensitive: false,
  includeScore: true,
  // shouldSort: true,
  // includeMatches: false,
  //   findAllMatches: false,
  // minMatchCharLength: 1,
  // location: 0,
  //    threshold: 0.6,
  distance: 1000,
  useExtendedSearch: true,
  // ignoreLocation: false,
  // ignoreFieldNorm: false,
  keys: [
    "arztNr",
    "arztFunktion",
    "arztTitel",
    "arztVorname",
    "arztName",
    "krhsName",
    "krhsStrNr",
    "krhsPlz",
    "krhsOrt",
    "fachgebiet",
    "fachgebiet1",
    "fachgebiet2",
    "fachgebiet3",
    "arztSuchbegriffe",
    "arztSpezielleKompetenz",
    "kommentar",
    "arztSpezielleKompetenz",
    "arztSuchbegriffe",
    "betreuer",
  ],
};

function App() {
  const [query, updateQuery] = useState("");
  const fuse = new Fuse(ärzte, options);

  const results = fuse.search(query);
  const arztResults = query ? results.map((arzt) => arzt.item) : ärzte;

  const [details, setDetails] = useState(0);

  function onSearch({ currentTarget }) {
    updateQuery(currentTarget.value);
  }

  function handleDetails() {
    setDetails(details + 1);
    if (details === 2) {
      setDetails(0);
    }
  }
  return (
    <>
      <Kopfzeile titel="Arztsuche" />

      <main className="container">
        <div className="ärzte">
          <button className="blauer-button" onClick={() => handleDetails()}>
            Details An/Aus/Liste
          </button>
          <button>
            <a
              className="blauer-button"
              href="http://blog.wenzlaff.de/?p=17180"
            >
              Online Hilfe
            </a>
          </button>
          <aside>
            <form className="search">
              <label>Suche</label>
              <input type="text" value={query} onChange={onSearch} />
            </form>
          </aside>

          <Suchfeld satz={ärzte} ergebnis={arztResults} />

          {arztResults.map((arzt) => {
            return <Karte satz={arzt} details={details} />;
          })}
        </div>
      </main>
      <Fusszeile titel="Bereich Hannover" />
    </>
  );
}

export default App;
