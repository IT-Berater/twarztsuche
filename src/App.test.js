import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('Online Hilfe exist', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Online Hilfe/i);
  expect(linkElement).toBeInTheDocument();
});
