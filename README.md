# TWArztSuche

React und Fuse.js Arztsuche auf Basis von Webpack.

 Mehr Infos unter http://blog.wenzlaff.de/?p=17161

# Install

git clone https://gitlab.com/IT-Berater/twarztsuche.git

cd twarztsuche

npm install

npm install webpack@latest
npm install html-webpack-plugin@latest

npm run build-dev

oder run build-prod

# Updarte

npx npm-check -u

# Test
npm run test

 ```
PASS  src/App.test.js
  ✓ Online Hilfe exist (50 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.353 s
Ran all test suites.

Watch Usage: Press w to show more.
```

# Start der Anwendung
npm run start

```
Compiled successfully!

You can now view twarztsuche in the browser.

  Local:            http://localhost:3000
  On Your Network:  http://10.10.10.10:3000

Note that the development build is not optimized.
To create a production build, use npm run build.
```

Es startet diese Anwendung


![](http://blog.wenzlaff.de/wp-content/uploads/2021/06/wenzlaff.de-2021-06-20-um-17.20.25.png)

Goals:
```
npm run clean --> "rm dist/main.js",
npm run start -->  "webpack serve --mode development --env development",
npm run compile -->  "webpack",
npm run build-prod -->  "webpack --mode production",
npm run build-dev -->  "webpack --mode development"
```

